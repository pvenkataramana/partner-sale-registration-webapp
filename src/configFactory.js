import Config from './config';
import {IdentityServiceSdkConfig} from 'identity-service-sdk';
import {SessionManagerConfig} from 'session-manager';
import {CustomerSegmentServiceSdkConfig} from 'customer-segment-service-sdk';
import {CustomerBrandServiceSdkConfig} from 'customer-brand-service-sdk';
import {AccountServiceSdkConfig} from 'account-service-sdk';
import {AccountContactServiceSdkConfig} from 'account-contact-service-sdk';
import {BuyingGroupServiceSdkConfig} from 'buying-group-service-sdk';
import {ManagementCompanyServiceSdkConfig} from 'management-company-service-sdk';
import {CustomerSourceServiceSdkConfig} from 'customer-source-service-sdk';
import {PartnerRepServiceSdkConfig} from 'partner-rep-service-sdk';
import {PartnerRepAssociationServiceSdkConfig} from 'partner-rep-association-service-sdk';
import {PartnerSaleRegistrationDraftServiceSdkConfig} from 'partner-sale-registration-draft-service-sdk';
import {ProductGroupServiceSdkConfig} from 'product-group-service-sdk';
import {ProductLineServiceSdkConfig} from 'product-line-service-sdk';
import {AssetServiceSdkConfig} from 'asset-service-sdk';
import {PartnerSaleInvoiceServiceSdkConfig} from 'partner-sale-invoice-service-sdk';


export default class ConfigFactory {

    /**
     * @param {object} data
     * @returns {Config}
     */
    static construct(data):Config {

        const identityServiceSdkConfig =
            new IdentityServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );


        const customerSegmentServiceSdkConfig =
            new CustomerSegmentServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const customerBrandServiceSdkConfig =
            new CustomerBrandServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const accountServiceSdkConfig =
            new AccountServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const accountContactServiceSdkConfig =
            new AccountContactServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const buyingGroupServiceSdkConfig =
            new BuyingGroupServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const managementCompanyServiceSdkConfig =
            new ManagementCompanyServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const customerSourceServiceSdkConfig =
            new CustomerSourceServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const partnerRepServiceSdkConfig =
            new PartnerRepServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );


        const partnerRepAssociationServiceSdkConfig =
            new PartnerRepAssociationServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const sessionManagerConfig =
            new SessionManagerConfig(
                data.precorConnectApiBaseUrl,
                data.sessionManagerConfig.loginUrl,
                data.sessionManagerConfig.logoutUrl
            );

        const partnerSaleRegistrationDraftServiceSdkConfig =
            new PartnerSaleRegistrationDraftServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const productGroupServiceSdkConfig =
            new ProductGroupServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const productLineServiceSdkConfig =
            new ProductLineServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const assetServiceSdkConfig =
            new AssetServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const partnerSaleInvoiceServiceSdkConfig =
            new PartnerSaleInvoiceServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );



        return new Config(
            identityServiceSdkConfig,
            customerSegmentServiceSdkConfig,
            customerBrandServiceSdkConfig,
            accountServiceSdkConfig,
            accountContactServiceSdkConfig,
            buyingGroupServiceSdkConfig,
            managementCompanyServiceSdkConfig,
            customerSourceServiceSdkConfig,
            partnerRepServiceSdkConfig,
            partnerRepAssociationServiceSdkConfig,
            sessionManagerConfig,
            partnerSaleRegistrationDraftServiceSdkConfig,
            productGroupServiceSdkConfig,
            productLineServiceSdkConfig,
            assetServiceSdkConfig,
            partnerSaleInvoiceServiceSdkConfig
        );

    }

}
