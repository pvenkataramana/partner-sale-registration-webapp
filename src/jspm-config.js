System.config({
  defaultJSExtensions: true,
  transpiler: "babel",
  babelOptions: {
    "optional": [
      "runtime",
      "optimisation.modules.system",
      "es7.decorators",
      "es7.classProperties"
    ]
  },
  paths: {
    "github:*": "jspm_packages/github/*",
    "npm:*": "jspm_packages/npm/*",
    "bitbucket:*": "jspm_packages/bitbucket/*"
  },

  map: {
    "account-contact-service-sdk": "bitbucket:precorconnect/account-contact-service-sdk-for-javascript@0.0.21",
    "account-service-sdk": "bitbucket:precorconnect/account-service-sdk-for-javascript@0.0.35",
    "add-partner-rep-modal": "bitbucket:precorconnect/add-partner-rep-modal-for-angularjs@0.0.11",
    "angular": "github:angular/bower-angular@1.4.8",
    "angular-bootstrap": "github:angular-ui/bootstrap-bower@0.13.4",
    "angular-messages": "github:angular/bower-angular-messages@1.4.8",
    "angular-route": "github:angular/bower-angular-route@1.4.8",
    "angular-strap": "github:mgcrea/angular-strap@2.3.6",
    "asset-service-sdk": "bitbucket:precorconnect/asset-service-sdk-for-javascript@0.0.35",
    "babel": "npm:babel-core@5.8.34",
    "babel-runtime": "npm:babel-runtime@5.8.34",
    "bootstrap": "github:twbs/bootstrap@3.3.6",
    "buying-group-service-sdk": "bitbucket:precorconnect/buying-group-service-sdk-for-javascript@0.0.1",
    "clean-css": "npm:clean-css@3.4.8",
    "core-js": "npm:core-js@1.2.6",
    "css": "github:systemjs/plugin-css@0.1.20",
    "customer-brand-service-sdk": "bitbucket:precorconnect/customer-brand-service-sdk-for-javascript@0.0.27",
    "customer-segment-service-sdk": "bitbucket:precorconnect/customer-segment-service-sdk-for-javascript@0.0.39",
    "customer-source-service-sdk": "bitbucket:precorconnect/customer-source-service-sdk-for-javascript@master",
    "danialfarid/ng-file-upload": "github:danialfarid/ng-file-upload@10.1.9",
    "footer": "bitbucket:precorconnect/footer-for-angularjs@0.0.11",
    "header": "bitbucket:precorconnect/header-for-angularjs@0.0.20",
    "identity-service-sdk": "bitbucket:precorconnect/identity-service-sdk-for-javascript@0.0.121",
    "iso-3166-sdk": "bitbucket:precorconnect/iso-3166-sdk-for-javascript@0.0.16",
    "json": "github:systemjs/plugin-json@0.1.0",
    "management-company-service-sdk": "bitbucket:precorconnect/management-company-service-sdk-for-javascript@0.0.8",
    "partner-rep-association-service-sdk": "bitbucket:precorconnect/partner-rep-association-service-sdk-for-javascript@0.0.4",
    "partner-rep-service-sdk": "bitbucket:precorconnect/partner-rep-service-sdk-for-javascript@0.0.29",
    "partner-sale-invoice-service-sdk": "bitbucket:precorconnect/partner-sale-invoice-service-sdk-for-javascript@0.0.30",
    "partner-sale-registration-draft-service-sdk": "bitbucket:precorconnect/partner-sale-registration-draft-service-sdk-for-javascript@0.0.4",
    "postal-object-model": "bitbucket:precorconnect/postal-object-model-for-javascript@0.0.12",
    "precorconnect/header-for-angularjs": "bitbucket:precorconnect/header-for-angularjs@0.0.20",
    "product-group-service-sdk": "bitbucket:precorconnect/product-group-service-sdk-for-javascript@0.0.14",
    "product-line-service-sdk": "bitbucket:precorconnect/product-line-service-sdk-for-javascript@0.0.22",
    "session-manager": "bitbucket:precorconnect/session-manager-for-browsers@0.0.55",
    "text": "github:systemjs/plugin-text@0.0.2",
    "bitbucket:precorconnect/account-contact-service-sdk-for-javascript@0.0.21": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0"
    },
    "bitbucket:precorconnect/account-service-sdk-for-javascript@0.0.35": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0",
      "postal-object-model": "bitbucket:precorconnect/postal-object-model-for-javascript@0.0.12"
    },
    "bitbucket:precorconnect/add-partner-rep-modal-for-angularjs@0.0.11": {
      "angular": "github:angular/bower-angular@1.4.8",
      "angular-bootstrap": "github:angular-ui/bootstrap-bower@0.13.4",
      "angular-messages": "github:angular/bower-angular-messages@1.4.8",
      "bootstrap": "github:twbs/bootstrap@3.3.6",
      "css": "github:systemjs/plugin-css@0.1.20",
      "partner-rep-service-sdk": "bitbucket:precorconnect/partner-rep-service-sdk-for-javascript@0.0.9",
      "session-manager": "bitbucket:precorconnect/session-manager-for-browsers@0.0.55",
      "text": "github:systemjs/plugin-text@0.0.2"
    },
    "bitbucket:precorconnect/asset-service-sdk-for-javascript@0.0.35": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0"
    },
    "bitbucket:precorconnect/buying-group-service-sdk-for-javascript@0.0.1": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0"
    },
    "bitbucket:precorconnect/customer-brand-service-sdk-for-javascript@0.0.27": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0"
    },
    "bitbucket:precorconnect/customer-segment-service-sdk-for-javascript@0.0.39": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0"
    },
    "bitbucket:precorconnect/customer-source-service-sdk-for-javascript@master": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0"
    },
    "bitbucket:precorconnect/footer-for-angularjs@0.0.11": {
      "angular": "github:angular/bower-angular@1.4.8",
      "css": "github:systemjs/plugin-css@0.1.20",
      "text": "github:systemjs/plugin-text@0.0.2"
    },
    "bitbucket:precorconnect/header-for-angularjs@0.0.20": {
      "angular": "github:angular/bower-angular@1.4.8",
      "bootstrap": "github:twbs/bootstrap@3.3.6",
      "css": "github:systemjs/plugin-css@0.1.20",
      "session-manager": "bitbucket:precorconnect/session-manager-for-browsers@0.0.54",
      "text": "github:systemjs/plugin-text@0.0.2"
    },
    "bitbucket:precorconnect/identity-service-sdk-for-javascript@0.0.111": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.10.3"
    },
    "bitbucket:precorconnect/identity-service-sdk-for-javascript@0.0.119": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0"
    },
    "bitbucket:precorconnect/identity-service-sdk-for-javascript@0.0.121": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0"
    },
    "bitbucket:precorconnect/iso-3166-sdk-for-javascript@0.0.16": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "iso-3166-2": "npm:iso-3166-2@0.2.3"
    },
    "bitbucket:precorconnect/management-company-service-sdk-for-javascript@0.0.8": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0"
    },
    "bitbucket:precorconnect/partner-rep-association-service-sdk-for-javascript@0.0.4": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0"
    },
    "bitbucket:precorconnect/partner-rep-service-sdk-for-javascript@0.0.29": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.12.1",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0",
      "postal-object-model": "bitbucket:precorconnect/postal-object-model-for-javascript@0.0.12"
    },
    "bitbucket:precorconnect/partner-rep-service-sdk-for-javascript@0.0.9": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0",
      "postal-object-model": "bitbucket:precorconnect/postal-object-model-for-javascript@0.0.12"
    },
    "bitbucket:precorconnect/partner-sale-invoice-service-sdk-for-javascript@0.0.30": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0"
    },
    "bitbucket:precorconnect/partner-sale-registration-draft-service-sdk-for-javascript@0.0.4": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0"
    },
    "bitbucket:precorconnect/product-group-service-sdk-for-javascript@0.0.14": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0"
    },
    "bitbucket:precorconnect/product-line-service-sdk-for-javascript@0.0.22": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0"
    },
    "bitbucket:precorconnect/session-manager-for-browsers@0.0.54": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "identity-service-sdk": "bitbucket:precorconnect/identity-service-sdk-for-javascript@0.0.111",
      "localforage": "npm:localforage@1.3.1",
      "uri": "github:medialize/URI.js@1.17.0"
    },
    "bitbucket:precorconnect/session-manager-for-browsers@0.0.55": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "identity-service-sdk": "bitbucket:precorconnect/identity-service-sdk-for-javascript@0.0.119",
      "localforage": "npm:localforage@1.3.1",
      "uri": "github:medialize/URI.js@1.17.0"
    },
    "github:angular/bower-angular-route@1.4.8": {
      "angular": "github:angular/bower-angular@1.4.8"
    },
    "github:aurelia/dependency-injection@0.12.1": {
      "aurelia-logging": "github:aurelia/logging@0.9.0",
      "aurelia-metadata": "github:aurelia/metadata@0.10.1",
      "aurelia-pal": "github:aurelia/pal@0.3.0",
      "core-js": "npm:core-js@1.2.6"
    },
    "github:aurelia/dependency-injection@0.9.2": {
      "aurelia-logging": "github:aurelia/logging@0.6.4",
      "aurelia-metadata": "github:aurelia/metadata@0.7.3",
      "core-js": "npm:core-js@0.9.18"
    },
    "github:aurelia/http-client@0.10.3": {
      "aurelia-path": "github:aurelia/path@0.8.1",
      "core-js": "npm:core-js@0.9.18"
    },
    "github:aurelia/http-client@0.11.0": {
      "aurelia-path": "github:aurelia/path@0.9.0",
      "core-js": "npm:core-js@0.9.18"
    },
    "github:aurelia/metadata@0.10.1": {
      "aurelia-pal": "github:aurelia/pal@0.3.0",
      "core-js": "npm:core-js@1.2.6"
    },
    "github:aurelia/metadata@0.7.3": {
      "core-js": "npm:core-js@0.9.18"
    },
    "github:jspm/nodelibs-assert@0.1.0": {
      "assert": "npm:assert@1.3.0"
    },
    "github:jspm/nodelibs-buffer@0.1.0": {
      "buffer": "npm:buffer@3.5.5"
    },
    "github:jspm/nodelibs-events@0.1.1": {
      "events": "npm:events@1.0.2"
    },
    "github:jspm/nodelibs-http@1.7.1": {
      "Base64": "npm:Base64@0.2.1",
      "events": "github:jspm/nodelibs-events@0.1.1",
      "inherits": "npm:inherits@2.0.1",
      "stream": "github:jspm/nodelibs-stream@0.1.0",
      "url": "github:jspm/nodelibs-url@0.1.0",
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "github:jspm/nodelibs-https@0.1.0": {
      "https-browserify": "npm:https-browserify@0.0.0"
    },
    "github:jspm/nodelibs-os@0.1.0": {
      "os-browserify": "npm:os-browserify@0.1.2"
    },
    "github:jspm/nodelibs-path@0.1.0": {
      "path-browserify": "npm:path-browserify@0.0.0"
    },
    "github:jspm/nodelibs-process@0.1.2": {
      "process": "npm:process@0.11.2"
    },
    "github:jspm/nodelibs-stream@0.1.0": {
      "stream-browserify": "npm:stream-browserify@1.0.0"
    },
    "github:jspm/nodelibs-url@0.1.0": {
      "url": "npm:url@0.10.3"
    },
    "github:jspm/nodelibs-util@0.1.0": {
      "util": "npm:util@0.10.3"
    },
    "github:mgcrea/angular-strap@2.3.6": {
      "angular": "github:angular/bower-angular@1.4.8"
    },
    "github:twbs/bootstrap@3.3.6": {
      "jquery": "github:components/jquery@2.1.4"
    },
    "npm:amdefine@1.0.0": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "module": "github:jspm/nodelibs-module@0.1.0",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:asap@1.0.0": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:assert@1.3.0": {
      "util": "npm:util@0.10.3"
    },
    "npm:babel-runtime@5.8.34": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:buffer@3.5.5": {
      "base64-js": "npm:base64-js@0.0.8",
      "child_process": "github:jspm/nodelibs-child_process@0.1.0",
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "ieee754": "npm:ieee754@1.1.6",
      "isarray": "npm:isarray@1.0.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:clean-css@3.4.8": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0",
      "commander": "npm:commander@2.8.1",
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "http": "github:jspm/nodelibs-http@1.7.1",
      "https": "github:jspm/nodelibs-https@0.1.0",
      "os": "github:jspm/nodelibs-os@0.1.0",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "source-map": "npm:source-map@0.4.4",
      "url": "github:jspm/nodelibs-url@0.1.0",
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:commander@2.8.1": {
      "child_process": "github:jspm/nodelibs-child_process@0.1.0",
      "events": "github:jspm/nodelibs-events@0.1.1",
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "graceful-readlink": "npm:graceful-readlink@1.0.1",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:core-js@0.9.18": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "systemjs-json": "github:systemjs/plugin-json@0.1.0"
    },
    "npm:core-js@1.2.6": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "systemjs-json": "github:systemjs/plugin-json@0.1.0"
    },
    "npm:core-util-is@1.0.2": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0"
    },
    "npm:graceful-readlink@1.0.1": {
      "fs": "github:jspm/nodelibs-fs@0.1.2"
    },
    "npm:https-browserify@0.0.0": {
      "http": "github:jspm/nodelibs-http@1.7.1"
    },
    "npm:inherits@2.0.1": {
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:localforage@1.3.1": {
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "promise": "npm:promise@5.0.0"
    },
    "npm:os-browserify@0.1.2": {
      "os": "github:jspm/nodelibs-os@0.1.0"
    },
    "npm:path-browserify@0.0.0": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:process@0.11.2": {
      "assert": "github:jspm/nodelibs-assert@0.1.0"
    },
    "npm:promise@5.0.0": {
      "asap": "npm:asap@1.0.0"
    },
    "npm:punycode@1.3.2": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:readable-stream@1.1.13": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0",
      "core-util-is": "npm:core-util-is@1.0.2",
      "events": "github:jspm/nodelibs-events@0.1.1",
      "inherits": "npm:inherits@2.0.1",
      "isarray": "npm:isarray@0.0.1",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "stream-browserify": "npm:stream-browserify@1.0.0",
      "string_decoder": "npm:string_decoder@0.10.31"
    },
    "npm:source-map@0.4.4": {
      "amdefine": "npm:amdefine@1.0.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:stream-browserify@1.0.0": {
      "events": "github:jspm/nodelibs-events@0.1.1",
      "inherits": "npm:inherits@2.0.1",
      "readable-stream": "npm:readable-stream@1.1.13"
    },
    "npm:string_decoder@0.10.31": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0"
    },
    "npm:url@0.10.3": {
      "assert": "github:jspm/nodelibs-assert@0.1.0",
      "punycode": "npm:punycode@1.3.2",
      "querystring": "npm:querystring@0.2.0",
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:util@0.10.3": {
      "inherits": "npm:inherits@2.0.1",
      "process": "github:jspm/nodelibs-process@0.1.2"
    }
  }
});
