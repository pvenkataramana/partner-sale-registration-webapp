import angular from 'angular';
import 'angular-route';
import 'angular-bootstrap';
import './js/ui-bootstrap-tpls';
import 'angular-messages';
import './bootstrap';
import 'header';
import 'footer';
import RouteConfig from './routeConfig';
import ConfigFactory from './configFactory';
import Config from './config';
import configData from './configData.json!json';
import IdentityServiceSdk from 'identity-service-sdk';
import SessionManager from 'session-manager';
import './style.css!css';
import './facilityModule';
import Iso3166Sdk from 'iso-3166-sdk';
import CustomerSegmentServiceSdk from  'customer-segment-service-sdk';
import CustomerBrandServiceSdk from 'customer-brand-service-sdk';
import AccountServiceSdk from 'account-service-sdk';
import AccountContactServiceSdk from 'account-contact-service-sdk';
import BuyingGroupServiceSdk from 'buying-group-service-sdk';
import ManagementCompanyServiceSdk from 'management-company-service-sdk';
import CustomerSourceServiceSdk from 'customer-source-service-sdk';
import PartnerRepServiceSdk from 'partner-rep-service-sdk';
import PartnerRepAssociationServiceSdk from 'partner-rep-association-service-sdk';
import PartnerSaleRegistrationServiceSdk from 'partner-sale-registration-draft-service-sdk';
import ProductGroupServiceSdk from 'product-group-service-sdk';
import ProductLineServiceSdk from 'product-line-service-sdk';
import AssetServiceSdk from 'asset-service-sdk';
import PartnerSaleInvoiceServiceSdk from 'partner-sale-invoice-service-sdk';
import ngFileUpload from 'danialfarid/ng-file-upload';
import 'add-partner-rep-modal';
import './directives/selectDirective';

angular
    .module(
        "productRegistrationWebApp.module",
        [   'ngRoute',
            'ui.bootstrap',
            'addPartnerRepModal.module',
            'productRegistrationCntrl',
            'productRegistrationPricingCntrl',
            'productRegistrationReviewCntrl',
            'productRegistrationSubmitCntrl',
            'customDateFormat',
            'ngMessages',
            'header.module',
            'footer.module',
            'facilityModule',
            'ngFileUpload'
        ]
    )
    .factory(
        'config',
        () => ConfigFactory.construct(configData)
    )
    .factory(
        'identityServiceSdk',
        [
            'config',
            config => new IdentityServiceSdk(config.identityServiceSdkConfig)
        ]
    )
    .factory(
        'iso3166Sdk', function() {
            return new Iso3166Sdk();

        }
    )
    .factory(
        'customerSegmentServiceSdk', [
            'config',
            config => new CustomerSegmentServiceSdk(config.customerSegmentServiceSdkConfig)
        ]
    )
    .factory(
        'customerBrandServiceSdk', [
            'config',
            config => new CustomerBrandServiceSdk(config.customerBrandServiceSdkConfig)
        ]
    )
    .factory(
        'accountServiceSdk', [
            'config',
            config => new AccountServiceSdk(config.accountServiceSdkConfig)
        ]
    )
    .factory(
        'accountContactServiceSdk', [
            'config',
            config => new AccountContactServiceSdk(config.accountContactServiceSdkConfig)
        ]
    )
    .factory(
        'buyingGroupServiceSdk', [
            'config',
            config => new BuyingGroupServiceSdk(config.buyingGroupServiceSdkConfig)
        ]
    )
    .factory(
        'managementCompanyServiceSdk', [
            'config',
            config => new ManagementCompanyServiceSdk(config.managementCompanyServiceSdkConfig)
        ]
    )
    .factory(
        'partnerRepServiceSdk', [
            'config',
            config => new PartnerRepServiceSdk(config.partnerRepServiceSdkConfig)
        ]
    )
    .factory(
        'partnerRepAssociationServiceSdk', [
            'config',
            config => new PartnerRepAssociationServiceSdk(config.partnerRepAssociationServiceSdkConfig)
        ]
    )
    .factory(
        'customerSourceServiceSdk', [
            'config',
            config => new CustomerSourceServiceSdk(config.customerSourceServiceSdkConfig)
        ]
    ).factory(
        'partnerSaleRegistrationServiceSdk', [
            'config',
            config => new PartnerSaleRegistrationServiceSdk(config.partnerSaleRegistrationDraftServiceSdkConfig)
        ]
    )
    .factory(
        'productGroupServiceSdk', [
            'config',
            config => new ProductGroupServiceSdk(config.productGroupServiceSdkConfig)
        ]
    )
    .factory(
        'productLineServiceSdk', [
            'config',
            config => new ProductLineServiceSdk(config.productLineServiceSdkConfig)
        ]
    )
    .factory(
        'assetServiceSdk', [
            'config',
            config => new AssetServiceSdk(config.assetServiceSdkConfig)
        ]
    )
    .factory(
        'partnerSaleInvoiceServiceSdk', [
            'config',
            config => new PartnerSaleInvoiceServiceSdk(config.partnerSaleInvoiceServiceSdkConfig)
        ]
    )
    .factory(
        'partnerRepServiceSdk',
        [
            'config',
            config => new PartnerRepServiceSdk(config.partnerRepServiceSdkConfig)
        ]
    )
    .factory(
        'sessionManager',
        [
            'config',
            config => new SessionManager(config.sessionManagerConfig)
        ]
    ).config(['$routeProvider', $routeProvider => new RouteConfig($routeProvider)]);