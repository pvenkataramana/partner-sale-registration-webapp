import IdentityServiceSdk from 'identity-service-sdk';
import SessionManager from 'session-manager';

angular
    .module('productRegistrationReviewCntrl',['ui.bootstrap']);
export default class productRegistrationReviewCntrl{
    constructor($scope,$q, $uibModal,$location,sessionManager, identityServiceSdk,partnerSaleRegistrationServiceSdk) {

        var getReview=JSON.parse(window.localStorage.getItem("reviewList"));
        $scope.productsReviewList=getReview;
        var draftId=window.localStorage.getItem("draftId");
        $scope.productRegistrationReview_Next=function(){
            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;
                    $q(
                        resolve =>
                            partnerSaleRegistrationServiceSdk
                                .submitPartnerCommercialSaleRegDraft(draftId, accessToken)
                                .then(
                                    response =>
                                        resolve(response)
                                )
                    ).then(PartnerCommercialSaleRegDraftView => {
                        $scope.loader=false;
                        console.log(' submit RESPONSE PartnerCommercialSaleRegDraftView::',PartnerCommercialSaleRegDraftView);
                        window.localStorage.setItem("SubmittedDraft",JSON.stringify(PartnerCommercialSaleRegDraftView));
                        $scope.modalInstance = $uibModal.open({
                            scope:$scope,
                            template: '<div class="modal-header"> <h4 class="modal-title">Success !</h4></div>' +
                            '<div class="modal-body">Review success </div>' +
                            '<div class="modal-footer">' +
                            '<button class="btn btn-primary" type="button" ng-click="review_confirm()">Ok</button></div>',
                            size:'sm',
                            backdrop : 'static'
                        });
                    });
                });

        };
        $scope.review_confirm=function(){
            $location.path("/productRegistrationSubmit");
        };
        $scope.productRegistrationReview_Previous=function(){
            $location.path("/productRegistrationPricing");
        }
        $scope.Back_Drafts=function(){
            $location.path("/");
        };
    }
};

productRegistrationReviewCntrl.$inject=[
    '$scope',
    '$q',
    '$uibModal',
    '$location',
    'sessionManager',
    'identityServiceSdk',
    'partnerSaleRegistrationServiceSdk'
];