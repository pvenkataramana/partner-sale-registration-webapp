import IdentityServiceSdk from 'identity-service-sdk';
import SessionManager from 'session-manager';

angular
    .module('productRegistrationViewDraftCntrl',['ui.bootstrap']);
export default class productRegistrationViewDraftCntrl{
    constructor($scope, $uibModal,$location) {
        console.log("calling View Draft Cntrl");
        $scope.isDisabled=true;
        $scope.ViewDisabled=true;
        $scope.productRegistration={};
        $scope.ViewDraft=JSON.parse(localStorage.getItem("EditedDrafts"));
        $scope.productRegistration.invoiceNumber=$scope.ViewDraft.invoiceNumber;
        $scope.productRegistration.installDate=$scope.ViewDraft.installDate;
        $scope.productRegistration.sellDate=$scope.ViewDraft.sellDate;
        $scope.Back_Drafts=function(){
            $location.path("/");
        }
    }

};

productRegistrationViewDraftCntrl.$inject=[
    '$scope',
    '$uibModal',
    '$location'
];